import json
import os
import re
import zipfile


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from module import judgment_parser, ckip_parser


def save_json(file_name, data):
    if '.json' not in file_name:
        file_name = file_name + '.json'
    with open(file_name, 'w', encoding='utf-8') as j_write:
        json.dump(data, j_write, ensure_ascii=False)
    print(file_name + '共有' + str(len(data)) + '篇')
    print('------' + file_name + ' is finished ------')


def load_json(file):
    json_list = os.listdir(file)
    target_data = dict()
    for json_name in json_list:
        with open(file + json_name, 'r', encoding='utf-8') as j_data:
            target_data[json_name] = json.load(j_data)
    return target_data


def search_case(path, case_type, start_time, end_time):
    file_list = os.listdir(path)

    # 讀取給定月份範圍內的zip file
    for file_name in filter(lambda time: int(time[:-4]) >= start_time
                                         and not int(time[:-4]) > end_time, file_list):
        load_target = zipfile.ZipFile(path + file_name, 'r')

        cases = dict()
        # 讀取每個zip file中的json文件
        for json_path in load_target.namelist():
            with load_target.open(json_path) as json_file:
                case = json_file.read()
                case_data = json.loads(case.decode("utf-8"))

                # 藉由拆解zip中路徑名得到案件所屬法院及案件時間
                # 拆解例子 ['202007', '臺灣花蓮地方法院刑事', 'HLDM', '108', '交簡上', '23', '20200701', '1.json']
                path_info = re.split('/|,', json_path)

                # 抽取目標檔案並存放於dict中
                if case_data['JTITLE'] in case_type and '地方' in path_info[1]:
                    # 將json內文解析後儲存
                    cases[json_path] = judgment_parser.parsing(case_data)
                else:
                    continue
        save_json(file_name[:-4], cases)


def re_parentheses(sent):
    idx = 0
    while sent[idx] != ')':
        if sent[idx] == '(':
            idx += re_parentheses(sent[idx:])
        else:
            idx += 1
    return idx


def ws_pos(data):
    key_word = ['事實及理由']

    # 讀取data中各月份篩選後資料
    for month in data.keys():
        temp_data = list()

        # 將各筆案號中的事實及理由段句子串起
        for case in data[month].keys():
            try:
                temp_data = temp_data + data[month][case]['事實及理由']
            except:
                continue

        # 透過ckip_classic online版處理parsing
        save_json(month, ckip_parser.parsing(temp_data))


def dep_parsing(path):
    json_list = os.listdir(path)
    for json_name in json_list:
        with open(path + json_name, 'r', encoding='utf-8') as j_data:
            ckip_parser.const_parser(json.load(j_data), json_name)
            print(json_name + ' is finished.')


def main_search():
    # 進行解析並抽取出2020 1月~12月間地方法院之竊盜案
    filepath = './zip_data/'
    start_time = 202001
    end_time = 202012
    case_type = ['竊盜']
    search_case(filepath, case_type, start_time, end_time)


def main_ws_pos():
    # 讀取已經篩選並解析過的檔案
    filtered_data = load_json('./filtered_old/')
    ws_pos(filtered_data)


def main_dep_parsing():
    dep_parsing('./parsed/')


if __name__ == '__main__':
    # main_search():
    # main_ws_pos():
    main_dep_parsing()

