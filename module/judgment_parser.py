import sys
import json
import regex as re
from pprint import pprint


def parse_judgment(judgment):
    # init
    result = dict()
    jfull_raw = judgment['JFULL'].splitlines()
    jtitle = judgment['JTITLE']
    result['案由'] = jtitle

    # build patterns
    titles = ['主文', '事實', '理由', '事實及理由', '事實及理由要領']
    pattern_title = '^\s*(' + '|'.join(['\s*'.join(title) for title in titles]) + ')\s*$'
    pattern_date = '^\s*中\s*華\s*民\s*國.*年.*月.*日\s*$'

    # divide section
    flag = None
    for num, line in enumerate(jfull_raw):
        # find query for online judgments searching
        # (https://law.judicial.gov.tw/FJUD/default.aspx, https://lawsnote.com)
        if num == 0:
            result['標題'] = []
            result['標題'].append(line)

        # sections
        if re.match(pattern_title, line) is not None:
            flag = re.sub('\s', '', line)
            result[flag] = list()
        elif re.match(pattern_date, line) is not None:
            flag = None
            break
        elif flag is not None:
            result[flag] = result[flag] + [line]

    if len(result.keys()) < 3:
        flag = None
        for num, line in enumerate(jfull_raw):
            # find query for online judgments searching
            # (https://law.judicial.gov.tw/FJUD/default.aspx, https://lawsnote.com)
            if num == 0:
                result['標題'] = []
                result['標題'].append(line)

            # sections
            if re.match('.\s*.\s*人', line) and flag is None:
                flag = '主文'
                result[flag] = list()
            elif re.match(pattern_date, line) is not None:
                flag = None
                break
            elif flag is not None:
                result[flag] = result[flag] + [line]

    return result

# other ways to resplit judgment
# def resplit_judgment(judgment):
#     # init
#     result = dict()
#
#     # resplit
#     for title, data in judgment.items():
#         merge = ''.join([line.strip() for line in data])
#         result[title] = [x.strip() for x in re.split('[。！？，；：]', merge) if len(x)]
#
#     return result


# def resplit_judgment_into_paragraph(judgment):
#     # init
#     result = dict()
#
#     # resplit
#     for title, text in judgment.items():
#         if title == '主文':
#             merge = ''.join([line.strip() for line in text])
#             result[title] = merge
#
#         else:
#             temp = ''
#             result[title] = []
#
#             for line in text:
#                 if temp is '':
#                     temp += re.sub('\s', '', line)
#                 else:
#                     # 判決書中各理由以    分段
#                     if re.search('\s{2,}.*', line):
#                         temp += re.sub('\s', '', line)
#                     else:
#                         result[title].append(temp)
#                         temp = re.sub('\s', '', line)
#             result[title].append(temp)
#     return result


def resplit_judgment_into_numbered_list(judgment):
    # init
    result = dict()
    r1  = ('①','②','③','④','⑤','⑥','⑦','⑧','⑨','⑩','⑪','⑫','⑬','⑭','⑮','⑯','⑰','⑱','⑲','⑳')
    r2  = ('⑴','⑵','⑶','⑷','⑸','⑹','⑺','⑻','⑼','⑽','⑾','⑿','⒀','⒁','⒂','⒃','⒄','⒅','⒆','⒇')
    r3  = ('Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ')
    r4  = ('壹、','貳、','參、','肆、','伍、','陸、','柒、','捌、','玖、','拾、')
    r5  = ('㈠','㈡','㈢','㈣','㈤','㈥','㈦','㈧','㈨','㈩')
    r6  = ('㊀', '㊁', '㊂', '㊃', '㊄', '㊅', '㊆', '㊇', '㊈', '㊉')
    r7  = ('❶', '❷', '❸', '❹', '❺', '❻', '❼', '❽', '❾', '❿', '⓫', '⓬', '⓭', '⓮', '⓯', '⓰', '⓱', '⓲', '⓳', '⓴')
    r8  = ('⒈', '⒉', '⒊', '⒋', '⒌', '⒍', '⒎', '⒏', '⒐', '⒑', '⒒', '⒓', '⒔', '⒕', '⒖', '⒗', '⒘', '⒙', '⒚', '⒛')
    r9  = ('⓵', '⓶', '⓷', '⓸', '⓹', '⓺', '⓻', '⓼', '⓽', '⓾')
    r10 = ('（一）', '（二）', '（三）', '（四）', '（五）', '（六）', '（七）', '（八）', '（九）', '（十）', '（十一）', '（十二）', '（十三）', '（十四）', '（十五）', '（十六）', '（十七）', '（十八）', '（十九）', '（二十）')
    r11 = ('（一）', '（二）', '（三）', '（四）', '（五）', '（六）', '（七）', '（八）', '（九）', '（十）', '（十一）', '（十二）', '（十三）', '（十四）', '（十五）', '（十六）', '（十七）', '（十八）', '（十九）', '（二十）')
    r12 = ('一、', '二、', '三、', '四、', '五、', '六、', '七、', '八、', '九、', '十、', '十一、', '十二、', '十三、', '十四、', '十五、', '十六、', '十七、', '十八、', '十九、', '二十 ')
    r13 = ('A.','B.','C.','D.','E.','F.','G.','H.','I.','J.','K.')
    r14 = ('1.','2.','3.','4.','5.','6.','7.','8.','9.','10.','11.','12.','13.','14.','15.','16.','17.','18.','19.','20.')
    r15 = ('甲、','乙、','丙、','丁、','戊、','己、','庚、','辛、','壬、','奎、')

    # resplit
    for title, text in judgment.items():
        # for 標題、案由
        if title == '標題' or title == '案由':
            merge = ''.join([line.strip() for line in text])
            result[title] = merge

        # for 主文
        if title == '主文':
            merge = ''.join([line.strip() for line in text])
            result[title] = merge

        # for others (事實、理由...)
        else:
            temp = ''
            result[title] = []

            for line in text:
                l = re.sub('\s', '', line)
                # start
                if temp == '':
                    temp += l
                else:
                    # pop temp to result
                    if l.startswith(r1+r2+r3+r4+r5+r6+r7+r8+r9+r10+r11+r12+r13+r14+r15):
                        result[title].append(temp)
                        temp = l
                    # push into temp
                    else:
                        temp += l
            result[title].append(temp)
    return result


def parsing(judgment, resplit=True):
    # parse judgment
    judgment_parsed = parse_judgment(judgment)

    # clean judgment
    judgment_resplit = resplit_judgment_into_numbered_list(judgment_parsed)

    if resplit:
        return judgment_resplit
    else:
        return judgment_parsed


if __name__ == '__main__':
    # init
    filename = 'data/PCDV,108,勞訴,192,20191225,1.json'

    # load json
    with open(filename, 'r', encoding='utf-8') as fread:
        judgment = json.load(fread)

    # parse json
    result = parsing(judgment)
    pprint(result)
